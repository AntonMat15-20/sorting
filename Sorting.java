package ru.maa.sorting;

import java.io.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Sorting {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\anton\\IdeaProjects\\sorting\\src\\ru\\maa\\sorting\\input.txt"));
        ArrayList<Integer> numbers = new ArrayList<>();
        String readedStr;
        while ((readedStr = reader.readLine()) != null) {
            numbers.add(Integer.valueOf(readedStr));
        }
        reader.close();
        ArrayList<TwoNumbers> numberAndFreq = new ArrayList<>();
        for (Integer numberFromList : numbers) {
            if (!isTwoNumbersArrayListContainsNumber(numberAndFreq, numberFromList)) {
                numberAndFreq.add(new TwoNumbers(numberFromList, Collections.frequency(numbers, numberFromList)));
            }
        }
        numberAndFreq.sort(new Comparator<TwoNumbers>() {
            @Override
            public int compare(TwoNumbers o1, TwoNumbers o2) {
                return o2.freqency - o1.freqency;
            }
        });
        BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\anton\\IdeaProjects\\sorting\\src\\ru\\maa\\sorting\\output.txt"));
        for (TwoNumbers currentNumberAndFreq : numberAndFreq) {
            while (currentNumberAndFreq.freqency-- > 0) {
                writer.write(currentNumberAndFreq.number + " ");
            }
        }
        writer.flush();
        writer.close();
    }

    /**
     * Проверяет, есть ли число в списке пар число-количество повторений
     * @param twoNumbersArrayList Список пар число-количество повторений
     * @param number число, по которому нужно проверить существование элемента
     * @return true, если элемент с таким числом есть в списке, false в обратном случае
     */
    private static boolean isTwoNumbersArrayListContainsNumber(ArrayList<TwoNumbers> twoNumbersArrayList, int number) {
        for (TwoNumbers currentTwoNumbers : twoNumbersArrayList) {
            if (currentTwoNumbers.number == number) {
                return true;
            }
        }
        return false;
    }
}